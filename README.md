### spoilerTags();

A plugin on Tumblr by [glenthemes](//glenthemes.tumblr.com) that shows a customized warning message for posts that contain specific tags.

* view post: [glenthemes.tumblr.com/post/648322958817902592/spoilertags](https://glenthemes.tumblr.com/post/648322958817902592/spoilertags)
* download + options: [github.com/glenthemes/spoilerTags](https://github.com/glenthemes/spoilerTags)
